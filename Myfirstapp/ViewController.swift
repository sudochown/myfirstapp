//
//  ViewController.swift
//  Myfirstapp
//
//  Created by timmcewan on 9/22/16.
//  Copyright © 2016 breakthelabel. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var firstlabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        firstlabel.text = "HELLO WORLD"
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

